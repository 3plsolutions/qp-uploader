const CONVERTORVERSION = "1.0.0.8";

const {
    app,
    BrowserWindow,
    ipcMain,
    Menu,
    Tray,
} = require('electron');
const path = require('path');
var ipc = require('electron').ipcMain;
const updater = require("electron-updater");
const log = require('electron-log');
const {
    autoUpdater
} = require("electron-updater");
const https = require('https');

const ua = require('universal-analytics');
const { JSONStorage } = require('node-localstorage');
const nodeStorage = new JSONStorage(app.getPath('userData'));

const {
    diff,
    addedDiff,
    deletedDiff,
    detailedDiff,
    updatedDiff
} = require("deep-object-diff");
const {
    download
} = require('electron-dl');
const csv = require('csv-parser');

var fs = require('fs');
var firebase = require("firebase-admin");

var serviceAccount = require("./ServiceAccountKey.json");

var os = require('os');
const {
    spawn,
    execFile
} = require('child_process');


let mainWindow;

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
log.info('App starting...');

ipcMain.on('download-item', async(event, {
    url
}) => {
    event.sender.send('download-success', url)
    console.log(url)
    const win = BrowserWindow.getFocusedWindow();
    const downloadOptions = {
        saveAs: true,
        openFolderWhenDone: true
    };
    console.log(await download(win, url, downloadOptions));
});

//Auto Launch on startup
var AutoLaunch = require('auto-launch');
var autoLauncher = new AutoLaunch({
    name: "quickParent uploader",
    path: 'C:\\QuickSuite\\QuickParent\\QP Uploader\\QP Uploader.exe',
});
// Checking if autoLaunch is enabled, if not then enabling it.
autoLauncher.isEnabled().then(function(isEnabled) {
    if (isEnabled) return;
    autoLauncher.enable();
}).catch(function(err) {
    throw err;
});


// Storage setup for 'holding' values
const Store = require('electron-store');
const store = new Store();


// Main create window function
function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 1000,
        height: 1250,
        frame: false,
        backgroundColor: '#FFF',
        icon: path.join(__dirname, '\\assets\\icon.png'),
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true

        },
        show: false
    });
    mainWindow.maximize();
    mainWindow.show();

    // Disable menu
    //mainWindow.setMenu(null)

    // and load the index.html of the app.
    //TODO - check if first run
    console.log('first-run');
    console.log(store.get('first-run'));
    if (store.get('first-run') == "" || store.get('first-run') == undefined) {
        //event.sender.send('Firstrun');
        console.log(store.get('first-run'));
        mainWindow.loadFile('index.html');
    } else {
        console.log(store.get('first-run'));
        mainWindow.loadFile('continuous.html');
    }

    // Open the DevTools.
    //mainWindow.webContents.openDevTools()

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });



}
let window;
let isQuiting;
let tray;

app.on('before-quit', function() {
    isQuiting = true;
    console.log('quitting');
});

function sendStatusToWindow(text, event) {
    log.info(text);
    mainWindow.webContents.send('message', text);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function() {
    // Create the Menu
    // var topmenu = Menu.buildFromTemplate([
    //     {
    //         label: 'Check for updates',
    //         click() { 
    //             autoUpdater.checkForUpdatesAndNotify(); 
    //         } 
    //     }
    // ])
    // Menu.setApplicationMenu(topmenu);

    tray = new Tray(path.join(__dirname, '\\assets\\tray.png'));
    tray.setContextMenu(Menu.buildFromTemplate([{
            label: 'Show QuickParent App',
            click: function() {
                mainWindow.show();
            }
        },
        {
            label: 'Quit QuickParent',
            click: function() {
                isQuiting = true;
                app.quit();
            }
        }
    ]));
    createWindow();

    mainWindow.on('close', function(event) {
        if (!isQuiting) {
            event.preventDefault();
            mainWindow.hide();
            event.returnValue = false;
        }
    });
});

// Quit when all windows are closed.
app.on('window-all-closed', function() {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') app.quit();
});

app.on('activate', function() {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) createWindow();
});

app.on('window-all-closed', () => {
    app.quit();
});


function quitandrestart() {
    app.relaunch();
    app.quit();
}

ipc.on('quitandrestart', function() {
    store.set('first-run', 'done');
    quitandrestart();
});

autoUpdater.on('checking-for-update', () => {
    sendStatusToWindow('Checking for update...');
})
autoUpdater.on('update-available', (info) => {
    sendStatusToWindow('Update available.');
})

autoUpdater.on('update-not-available', (info) => {
    sendStatusToWindow('Update not available.');
})
autoUpdater.on('error', (err) => {
    sendStatusToWindow('Error in auto-updater. ' + err);
})
autoUpdater.on('download-progress', (progressObj) => {
    let log_message = "Download speed: " + formatBytes(progressObj.bytesPerSecond) + "/s";
    log_message = log_message + ' - Downloaded ' + progressObj.percent.toFixed() + '%';
    log_message = log_message + ' (' + formatBytes(progressObj.transferred) + "/" + formatBytes(progressObj.total) + ')';
    sendStatusToWindow(log_message);
})
autoUpdater.on('update-downloaded', (info) => {
    sendStatusToWindow('update_downloaded');
});


app.on('ready', function() {
    autoUpdater.checkForUpdatesAndNotify();
});


ipc.on("restart_app", () => {
    autoUpdater.quitAndInstall();
});

ipc.on("reset", () => {
    store.set('first-run', '');
    app.relaunch();
    app.quit();
});


firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://qptestsetup.firebaseio.com"
});

const settings = {
    timestampsInSnapshots: true
};
const db = firebase.firestore();

var userlist = '';
var parentsadded = 0;
var studentsadded = 0;
var academicdataadded = 0;
var academictasksadded = 0;
var academictasktext = '';
var academicdatatext = '';
var academicabsencesadded = 0;
var academicdisciplineadded = 0;
var currentlineparents = 0
var currentlinestudents = 0
var currentlineschool = 0
var currentlineAD = 0
var currentlineAT = 0
var currentlineAbsence = 0
var currentlineDiscipline = 0

var parentsInteger = 0;
var studentInteger = 0;
var reportsInteger = 0;
var tasksInteger = 0;
var absencesInteger = 0;
var disciplineInteger = 0;

var importerVal = 0;

var userobj = [{}];
db.settings(settings);
var listlog = Array;

function isDirSync(aPath) {
    try {
        return fs.statSync(aPath);
    } catch (e) {
        if (e.code === 'ENOENT') {
            return false;
        } else {
            throw e;
        }
    }
}




ipc.on('runconvertor', function(event, data) {

    // if convertor installed
    // check version
    // if out of date - continue..
    // if current skip

    if (isDirSync('C:/QuickSuite/QuickParent/QuickParentConverter.exe')) {
        console.log('Found file');
        if (isDirSync('C:/QuickSuite/QuickParent/ver.txt')) {
            fs.readFile('C:/QuickSuite/QuickParent/ver.txt', (error, txtString) => {
                if (error) throw err;
                console.log(txtString.toString());
                if (txtString.toString() != CONVERTORVERSION) {
                    installconvertor(event);
                }
            })
        }
        var returnedtext = "Data Extrator installed.";
        event.sender.send('noerrorimport', returnedtext);
    } else {
        installconvertor(event);
    }

});


function installconvertor(event) {

    var fileName = 'QP_CSV_Generator.msi';
    var sh = require("shelljs");
    var curwordir = sh.pwd();
    var child = spawn('cmd', ["/S /C " + fileName], {
        detached: true,
        cwd: curwordir.stdout,
        env: process.env
    });

    child.on('close', function(code) {
        console.log("Child process exited with code " + code);
        if (code == 1602) {
            var returnedtext = "Setup of the Data Extrator didn't install correctly";
            console.log(returnedtext);
            event.sender.send('errorimport', returnedtext);
        }
        if (code == 0) {
            var returnedtext = "The Data Extractor has been closed";
            event.sender.send('noerrorimport', returnedtext);
        }
    });

    child.unref();

};

var usr = null;
var schooluserId = null;

ipc.on('runimporter', function(event, data) {
    var fileName = 'C:/QuickSuite/QuickParent/QuickParentConverter.exe';
    var sh = require("shelljs");
    var curwordir = sh.pwd();
    //spawn command line (cmd as first param to spawn)
    var child = spawn('cmd', ["/S /C " + fileName], { // /S strips quotes and /C executes the runnable file (node way)
        detached: true, //see node docs to see what it does
        env: process.env
    });


    child.on('close', function(code) {
        console.log("Child process exited with code " + code);
        if (code == 1602) {
            var returnedtext = "Extrating the data from SAMS failed";
            console.log(returnedtext);
            event.sender.send('errorimport', returnedtext);
        }
        if (code == 0) {
            var returnedtext = "Extrating the data from SAMS completed correctly. Please continue.";
            event.sender.send('continueimport', returnedtext);

        }
    });


    // THIS IS TAKEN FROM NODE JS DOCS
    // By default, the parent will wait for the detached child to exit.
    // To prevent the parent from waiting for a given child, use the child.unref() method,
    // and the parent's event loop will not include the child in its reference count.
    var checkExist = setInterval(function() {
        console.log("interval loop");
        isRunning('QuickParentConverter.exe', (status) => {
            console.log("status");
            console.log(status); // true|false
        })
        if (isDirSync('C:/QuickSuite/QuickParent/ver.txt')) {
            var returnedtext = "Extrating the data from SAMS completed correctly. Please continue.";
            event.sender.send('continueimport', returnedtext);
            console.log("interval done");
            clearInterval(checkExist);
            child.unref();


            // (re)save the userid, so it persists for the next app session.


        }
    }, 1000); // check every 100ms


})

function setschooluserId(category, action, label, value) {
    var EMISID = "";
    fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv', fs.F_OK, (err) => {
        if (err) {
            fs.createReadStream('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old.csv', 'utf-8')
                .pipe(csv())
                .on('data', (row) => {

                    EMISID = row.EmisCode
                    console.log("EMISID1");
                    console.log(EMISID);
                    nodeStorage.setItem('userid', EMISID);
                    usr = ua('UA-156149023-1', EMISID);
                    trackEvent(category, action, label, value);
                })
        } else {
            fs.createReadStream('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv', 'utf-8')
                .pipe(csv())
                .on('data', (row) => {


                    EMISID = row.EmisCode
                    nodeStorage.setItem('userid', EMISID);
                    usr = ua('UA-156149023-1', EMISID);
                    trackEvent(category, action, label, value);
                })
        }
    })


}

function trackEvent(category, action, label, value) {
    usr
        .event({
            ec: category,
            ea: action,
            el: label,
            ev: value,
        })
        .send();
}

//Parent 'Run'
ipc.on('invokeParentAction', function(event, data) {
    setschooluserId('Uploader', 'Extracted', 'CSV data', 'completed');

    //TODO add back in..
    //    importParents(event);

    event.sender.send('ParentFinished', parentsadded);

});

//Student 'Run'

ipc.on('invokeStartImport', function(event, data) {

    importerVal = 0;
    studentImport(event);
    reportImport(event);
    taskImport(event);
    importAbsences(event);
    importDiscipline(event);


});

//School 'Run'
ipc.on('invokeSchoolAction', function(event, data) {
    importSchool(event);

    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\newparents-first.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\newparents.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-first.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-first.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-first.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-first.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-first.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-first.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv");
});


// FUNCTIONS

function importParents(event) {
    var parentnumberstext = '';
    var linecount = 0;
    var datenow = Date.now();
    fs.rename('C:\\QuickSuite\\QuickParent\\CSVData\\newparents.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\oldparents.csv' + datenow, (err) => {
        if (err) {
            console.log(err);
        }
        console.log('Rename complete!');
    });
    fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo-old.csv', fs.F_OK, (err) => {
        if (err) {
            console.error("parent old file doesnt exist");
            //file doesn't exists

            //count lines for progress bar
            filePath = 'C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo.csv';
            fileBuffer = fs.readFileSync(filePath);
            to_string = fileBuffer.toString();
            split_lines = to_string.split("\n");
            linecount = split_lines.length - 2;


            //just upload the original data
            fs.createReadStream('C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo.csv', 'utf-8')
                .pipe(csv())
                .on('data', (row) => {
                    currentlineparents = currentlineparents + 1;
                    var parent_progress = (currentlineparents / linecount).toFixed(2) * 100 + "%";
                    event.sender.send('ParentProgress', parent_progress);
                    var parentnumberstext = currentlineparents + ' of ' + linecount + '. ' + parentsadded + ' new parents added.';
                    event.sender.send('parentnumbersReply', parentnumberstext);

                    //TODO ADD BACK IN
                    // addnewparentuser(row, event, linecount, currentlineparents);
                    //ADD FUNCTION IN 
                    firebase.auth().getUserByEmail(row.EMail).then(user => {
                        // User already exists
                        var tablefields = "";
                        let newparent = db.collection("users").doc(row.EmisStamp + "-" + row.ParentID).set({
                            ParentID: row.ParentID,
                            EMail: row.EMail,
                            ParentID: row.ParentID,
                            Display_Name: row.Display_Name,
                            StreetAddress1: row.StreetAddress1,
                            StreetAddress2: row.StreetAddress2,
                            StreetAddress3: row.StreetAddress3,
                            StreetCode: row.StreetCode,
                            PostalAddress1: row.PostalAddress1,
                            PostalAddress2: row.PostalAddress2,
                            PostalAddress3: row.PostalAddress3,
                            PostalCode: row.PostalCode,
                            Tel1Code: row.Tel1Code,
                            Tel1: row.Tel1,
                            Tel2Code: row.Tel2Code,
                            Tel2: row.Tel2,
                            Tel3Code: row.Tel3Code,
                            Tel3: row.Tel3,
                            IDNumber: row.IDNumber,
                            Title: row.Title,
                            Homelanguage: row.Homelanguage,
                            Status: row.StatusParent,
                            EmisStamp: row.EmisStamp
                        }, {
                            merge: true
                        }).then(function() {
                            if (store.get('first-run') != "done") {
                                event.sender.send('ParentFinished', parentsadded);
                            }
                        });
                        trackEvent('Uploader', 'Import', 'Parent', 'completed');
                    }).catch(err => {
                        if (err.code === 'auth/user-not-found') {
                            // User doesn't exist yet, create it...
                            parentsadded = parentsadded + 1;
                            var phone_string = '';
                            if (row.phone_number != null) {
                                phone_string = 'phoneNumber: ' + row.phone_number + ',';
                            }
                            console.log(row);
                            var randPassword = generatePassword();
                            console.log('password: ' + randPassword);

                            var email_pass = row.EMail + ', ' + randPassword;
                            userobj.push({
                                EmisStamp: row.EmisStamp + '-' + row.ParentID,
                                email: row.EMail,
                                pass: randPassword
                            })
                            var tablefields = row.EMail + '</p></div><div class="w-1/4 h-8 text-right "><p class="text-sm text-grey-400 text-left">' + randPassword;

                            //TODO 
                            //appendfile lookup and cleanup..
                            var filename = 'C:\\QuickSuite\\QuickParent\\CSVData\\newparents.csv';
                            fs.access(filename, fs.F_OK, (err) => {
                                if (err) {
                                    console.error(err);
                                    fs.writeFileSync(filename, email_pass + '\r\n', function(err) {
                                        if (err) throw err;
                                        //  console.log('File is created successfully.');
                                    });
                                    return;
                                } else {
                                    fs.appendFileSync(filename, email_pass + '\r\n');
                                }
                            });
                            firebase.auth().createUser({
                                    uid: row.EmisStamp + '-' + row.ParentID,
                                    email: row.EMail,
                                    emailVerified: false,
                                    password: randPassword,
                                    displayName: row.Display_Name,
                                    disabled: false
                                })
                                .then(function(userRecord) {
                                    // listlog[i] = 'Successfully created new user:', userRecord.uid;
                                    trackEvent('Uploader', 'Import', 'New Auth Parent', 'completed');
                                })
                                .catch(function(error) {
                                    console.log('Error creating new user:/n', error);
                                });

                            let newparent = db.collection("users").doc(row.EmisStamp + "-" + row.ParentID).set({
                                ParentID: row.ParentID,
                                EMail: row.EMail,
                                ParentID: row.ParentID,
                                Display_Name: row.Display_Name,
                                StreetAddress1: row.StreetAddress1,
                                StreetAddress2: row.StreetAddress2,
                                StreetAddress3: row.StreetAddress3,
                                StreetCode: row.StreetCode,
                                PostalAddress1: row.PostalAddress1,
                                PostalAddress2: row.PostalAddress2,
                                PostalAddress3: row.PostalAddress3,
                                PostalCode: row.PostalCode,
                                Tel1Code: row.Tel1Code,
                                Tel1: row.Tel1,
                                Tel2Code: row.Tel2Code,
                                Tel2: row.Tel2,
                                Tel3Code: row.Tel3Code,
                                Tel3: row.Tel3,
                                IDNumber: row.IDNumber,
                                Title: row.Title,
                                Homelanguage: row.Homelanguage,
                                Status: row.StatusParent,
                                EmisStamp: row.EmisStamp
                            }, {
                                merge: true
                            }).then(function() {
                                // console.log("Parent created");
                                if (store.get('first-run') != "done") {
                                    event.sender.send('ParentFinished', parentsadded);
                                }
                            });

                            event.sender.send('ParentReply', tablefields);
                            var parentnumberstext = currentlineparents + ' of ' + linecount + '. ' + parentsadded + ' new parents added.';
                            event.sender.send('parentnumbersReply', parentnumberstext);

                            //TODO
                            trackEvent("parent", "created", "new", "new parent created");

                        } else {
                            var parentnumberstext = currentlineparents + ' of ' + linecount + '. ' + parentsadded + ' new parents added.';
                            //  console.log(parentnumberstext);
                            event.sender.send('parentnumbersReply', parentnumberstext);
                        }
                    })

                })
                .on('end', () => {
                    console.log('User/Parent CSV file successfully processed');

                    fs.copyFile('C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo-old.csv', (err) => {
                        if (err) {
                            console.log(err);
                        }
                        console.log('Rename complete!');
                    });
                    trackEvent('Uploader', 'Import', 'Parent', 'completed');
                    event.sender.send('ParentFinished', parentsadded);
                });
        } else {
            //file exists
            readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo.csv', 'userdata');
            event.sender.send('ParentFinished', parentsadded);
        }
    });
}



function studentImport(event) {
    var linecount = 0;
    let datas = [];
    let datassp = [];
    let datasp = [];

    var datacollection = "children";
    var datenow = Date.now();

    fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-old.csv', fs.F_OK, (err) => {
        if (err) {
            //file doesn't exists
            console.error("student old file doesnt exist");

            //count lines for progress bar
            filePath = 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo.csv';
            fileBuffer = fs.readFileSync(filePath);
            to_string = fileBuffer.toString();
            split_lines = to_string.split("\n");
            linecount = split_lines.length - 2;

            //just upload the original data
            fs.createReadStream('C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo.csv', 'utf-8')
                .pipe(csv())
                .on('data', (row) => {
                    currentlinestudents = currentlinestudents + 1;
                    studentnumberstext = currentlinestudents + ' of ' + linecount + '. ' + studentsadded + ' new student added or updated.';
                    var student_progress = (currentlinestudents / linecount).toFixed(2) * 100;
                    student_progress = student_progress + "%";
                    event.sender.send('StudentProgress', student_progress);
                    event.sender.send('studentnumbersReply', studentnumberstext);
                    let data = {};
                    let datastudentparent = {};
                    let dataparent = {};

                    data['Learnerid'] = row.Learnerid;
                    data['LearnerFName'] = row.LearnerFName;
                    data['LearnerSName'] = row.LearnerSName;
                    data['Grade'] = row.Grade;
                    data['ChildId'] = row.ChildId;
                    data['Tel1Code'] = row.Tel1Code;
                    data['Tel1'] = row.Tel1;
                    data['Tel2Code'] = row.Tel2Code;
                    data['Tel2'] = row.Tel2;
                    data['Tel3Code'] = row.Tel3Code;
                    data['Tel3'] = row.Tel3;
                    data['Status'] = row.Status;
                    data['BirthDate'] = row.BirthDate;
                    data['IDNo'] = row.IDNo;
                    data['Gender'] = row.Gender;
                    data['HomeLanguage'] = row.HomeLanguage;
                    data['Address1'] = row.Address1;
                    data['Address2'] = row.Address2;
                    data['Address3'] = row.Address3;
                    data['AddressCode'] = row.AddressCode;
                    data['ClassName'] = row.ClassName;
                    data['EmisStamp'] = row.EmisStamp;
                    data['docid'] = row.EmisStamp + "-" + row.Learnerid;
                    datastudentparent['docid'] = row.EmisStamp + "-" + row.Learnerid;

                    datastudentparent['ParentId'] = firebase.firestore.FieldValue.arrayUnion(row.EmisStamp + "-" + row.ParentId);
                    dataparent['ChildId'] = firebase.firestore.FieldValue.arrayUnion(row.EmisStamp + "-" + row.Learnerid);
                    dataparent['docid'] = row.EmisStamp + "-" + row.ParentId;

                    datas.push(data);
                    datassp.push(datastudentparent);
                    datasp.push(dataparent);
                    studentsadded = studentsadded + 1;

                    studentnumberstext = currentlinestudents + ' of ' + linecount + '. ' + studentsadded + ' new student added or updated.';
                    if (store.get('first-run') != "done") {
                        event.sender.send('studentnumbersReply', studentnumberstext);
                    }


                    trackEvent('Uploader', 'Import', 'Student', 'completed');

                    //END
                })
                .on('end', () => {
                    console.log('Student CSV file successfully processed');

                    fs.copyFile('C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-old.csv', (err) => {
                        if (err) {
                            console.log(err);
                        }
                        console.log('Rename complete!');
                    });

                    importerVal = importerVal + 1;
                    event.sender.send('importerVar', importerVal);
                    console.log("importerVal student=" + importerVal);
                    // console.log(datas);
                    // console.log(datasp);
                    // console.log(datassp);
                    BatchedWrites(datas, datacollection);


                });
        } else {
            //file exists
            readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo.csv', 'studentdata');
        }
    });
}

function reportImport(event) {
    fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-old.csv', fs.F_OK, (err) => {
        var linecount = 0;
        let datas = [];
        var datacollection = "children";
        var datacollection2 = 'reports';

        var datenow = Date.now();
        if (err) {
            //file doesn't exists
            console.error("learner report old file doesnt exist");

            //count lines for progress bar
            filePath = 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData.csv';
            fileBuffer = fs.readFileSync(filePath);
            to_string = fileBuffer.toString();
            split_lines = to_string.split("\n");
            linecount = split_lines.length - 2;

            if (linecount > 1) {
                //just upload the original data
                fs.createReadStream('C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData.csv', 'utf-8')
                    .pipe(csv())
                    .on('data', (row) => {
                        let data = {};

                        currentlineAD = currentlineAD + 1;
                        academicdatatext = currentlineAD + ' of ' + linecount + '. ' + academicdataadded + ' new academic reports uploaded.';
                        event.sender.send('academicdataReply', academicdatatext);
                        var academics_progress = (currentlineAD / linecount).toFixed(2) * 100 + "%";
                        event.sender.send('AcademicProgress', academics_progress);
                        //addacademicdata(row, event, linecount, currentlineAD);

                        //Function added in..

                        academicdataadded = academicdataadded + 1;
                        academicdatatext = currentlineAD + ' of ' + linecount + '. ' + academicdataadded + ' new academic reports uploaded.';
                        if (store.get('first-run') != "done") {
                            event.sender.send('academicdataReply', academicdatatext);
                        }

                        data['year'] = row.Datayear;
                        data['LearnerID'] = row.LearnerID;
                        data['AccessionNo'] = row.AccessionNo;
                        data['SName'] = row.SName;
                        data['FName'] = row.FName;
                        data['Grade'] = row.Grade;
                        data['Register'] = row.Register;
                        data['SubjectId'] = row.SubjectId;
                        data['subject'] = row.SubjectName;
                        data['subjectShort'] = row.SubjectShort;
                        data['term'] = row.Term;
                        data['mark'] = row.Mark;
                        data['totalMark'] = row.TotalMark;
                        data['Rating'] = row.Rating;
                        data['comment1'] = row.Comment1;
                        data['EmisStamp'] = row.EmisStamp;
                        data['docid'] = row.EmisStamp + "-" + row.AccessionNo;
                        data['docfinal'] = 'report-' + row.Datayear + "-" + row.Term + "-" + row.SubjectId;

                        datas.push(data);
                        trackEvent('Uploader', 'Import', 'Report', 'completed');

                    })
                    .on('end', () => {
                        console.log('Report CSV file successfully processed');

                        fs.copyFile('C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-old.csv', (err) => {
                            if (err) {
                                console.log(err);
                            }
                            console.log('Rename complete!');
                        });
                        importerVal = importerVal + 1;
                        event.sender.send('importerVar', importerVal);
                        console.log("importerVal report=" + importerVal);

                        BatchedWritesreports(datas, datacollection, datacollection2);

                    });
            }
        } else {
            //file exists
            readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData.csv', 'reportdata');
        }
    });
}

function taskImport(event) {
    var datenow = Date.now();

    let datas = [];
    var datacollection = "children";
    var datacollection2 = 'tasks';

    fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-old.csv', fs.F_OK, (err) => {
        var linecount = 0;
        if (err) {
            //file doesn't exists
            console.error("learner data old file doesnt exist");

            //count lines for progress bar
            filePath = 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData.csv';
            fileBuffer = fs.readFileSync(filePath);
            to_string = fileBuffer.toString();
            split_lines = to_string.split("\n");
            linecount = split_lines.length - 2;
            if (linecount > 1) {

                //just upload the original data
                fs.createReadStream('C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData.csv', 'utf-8')
                    .pipe(csv())
                    .on('data', (row) => {
                        let data = {};
                        currentlineAT = currentlineAT + 1;
                        var tasks_progress = (currentlineAT / linecount).toFixed(2) * 100 + "%";
                        event.sender.send('TasksProgress', tasks_progress);
                        academictasktext = currentlineAT + ' of ' + linecount + '. ' + academictasksadded + ' new academic tasks uploaded.';
                        event.sender.send('academictasksReply', academictasktext);

                        //fucntion added in
                        academictasksadded = academictasksadded + 1;
                        academictasktext = currentlineAT + ' of ' + linecount + '. ' + academictasksadded + ' new academic tasks uploaded.';
                        if (store.get('first-run') != "done") {
                            event.sender.send('academictasksReply', academictasktext);
                        }

                        data['year'] = row.Datayear;
                        data['LearnerID'] = row.Learnerid;
                        data['AccessionNo'] = row.AccessionNo;
                        data['SName'] = row.SName;
                        data['FName'] = row.FName;
                        data['Grade'] = row.Grade;
                        data['Register'] = row.Register;
                        data['subjectId'] = row.Subjectid;
                        data['subject'] = row.SubjectName;
                        data['subjectNameAfrs'] = row.SubjectNameAfrs;
                        data['subjectShort'] = row.SubjectShort;
                        data['TaskID'] = row.TaskID;
                        data['task'] = row.TaskName;
                        data['SubjectName'] = row.SubjectName;
                        data['mark'] = row.TaskMark;
                        data['taskTotal'] = row.TaskTotal;
                        data['term'] = row.Term;
                        data['EmisStamp'] = row.EmisStamp;
                        data['docid'] = row.EmisStamp + "-" + row.AccessionNo;
                        data['docfinal'] = 'task-' + row.Datayear + "-" + row.Term + "-" + row.Subjectid + "-" + row.TaskID;

                        datas.push(data);
                        trackEvent('Uploader', 'Import', 'Task', 'completed');


                    })
                    .on('end', () => {
                        console.log('Task CSV file successfully processed');

                        fs.copyFile('C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-old.csv', (err) => {
                            if (err) {
                                console.log(err);
                            }
                            console.log('Rename complete!');
                        });

                        importerVal = importerVal + 1;
                        event.sender.send('importerVar', importerVal);
                        console.log("importerVal task=" + importerVal);

                        BatchedWritesreports(datas, datacollection, datacollection2);

                    });
            }
        } else {
            //file exists
            readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData.csv', 'taskdata');
        }
    });
}

function importAbsences(event) {
    var datenow = Date.now();
    var linecount = 0;
    //Check  file exists
    fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-old.csv', fs.F_OK, (err) => {
        if (err) {

            console.error("Absence old file doesnt exist");

            filePath = 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount.csv';
            fileBuffer = fs.readFileSync(filePath);
            to_string = fileBuffer.toString();
            split_lines = to_string.split("\n");
            linecount = split_lines.length - 2;

            if (linecount > 1) {

                //just upload the original data
                fs.createReadStream('C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount.csv', 'utf-8')
                    .pipe(csv())
                    .on('data', (row) => {
                        currentlineAbsence = currentlineAbsence + 1;
                        var absences_progress = (currentlineAbsence / linecount).toFixed(2) * 100;
                        absences_progress = absences_progress + "%";
                        event.sender.send('absencesProgress', absences_progress);
                        academicabsencetext = currentlineAbsence + ' of ' + linecount + '. ' + academicabsencesadded + ' new academic absences uploaded.';
                        event.sender.send('academicabsencesReply', academicabsencetext);

                        academicabsencesadded = academicabsencesadded + 1;
                        academicabsencetext = currentlineAbsence + ' of ' + linecount + '. ' + academicabsencesadded + ' new academic absences uploaded.';
                        if (store.get('first-run') != "done") {
                            event.sender.send('academicabsencesReply', academicabsencetext);
                        }
                        var addAbsenceCollection = db.collection("children").doc(row.EmisStamp + "-" + row.AccessionNo).collection("absences");
                        var absencedatestripped = row.AbsentDate.replace(/\D/g, '');
                        var AbsenceData = addAbsenceCollection.doc('absence-' + row.DataYear + "-" + absencedatestripped).set({
                            year: row.DataYear,
                            QPID: row.QPID,
                            AbsenceDate: row.AbsentDate,
                            Reason: row.Reason,
                            EmisStamp: row.EmisStamp,
                            Status: row.Status,
                            read: false
                        }, {
                            merge: true
                        }).catch(function(error) {
                            console.error("Error adding document: ", error);
                        });
                        trackEvent('Uploader', 'Import', 'Absence', 'completed');

                    })
                    .on('end', () => {
                        console.log('Absence CSV file successfully processed');

                        fs.copyFile('C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-old.csv', (err) => {
                            if (err) {
                                console.log(err);
                            }
                            console.log('Rename complete!');
                        });
                        importerVal = importerVal + 1;
                        event.sender.send('importerVar', importerVal);
                        console.log("importerVal absences=" + importerVal);


                    });
            }
        } else {
            //file exists
            readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount.csv', 'absencedata');
        }
    });
}

function importDiscipline(event) {
    var datenow = Date.now();
    var linecount = 0;
    //Check schooldata2 file exists
    fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-old.csv', fs.F_OK, (err) => {
        if (err) {

            console.error("Discipline old file doesnt exist");

            filePath = 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline.csv';
            fileBuffer = fs.readFileSync(filePath);
            to_string = fileBuffer.toString();
            split_lines = to_string.split("\n");
            linecount = split_lines.length - 2;
            if (linecount > 1) {


                //just upload the original data
                fs.createReadStream('C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline.csv', 'utf-8')
                    .pipe(csv())
                    .on('data', (row) => {
                        currentlineDiscipline = currentlineDiscipline + 1;
                        //TODO add back in
                        var discipline_progress = (currentlineDiscipline / linecount).toFixed(2) * 100;
                        discipline_progress = discipline_progress + "%";
                        event.sender.send('disciplineProgress', discipline_progress);
                        academicdisciplinetext = currentlineDiscipline + ' of ' + linecount + '. ' + academicdisciplineadded + ' new academic discipline uploaded.';
                        event.sender.send('academicdisciplineReply', academicdisciplinetext);
                        // function added in
                        academicdisciplineadded = academicdisciplineadded + 1;
                        academicdisciplinetext = currentlineDiscipline + ' of ' + linecount + '. ' + academicdisciplineadded + ' new academic discipline uploaded.';
                        if (store.get('first-run') != "done") {
                            event.sender.send('academicdisciplineReply', academicdisciplinetext);
                        }
                        var adddisciplineCollection = db.collection("children").doc(row.EmisStamp + "-" + row.AccessionNo).collection("discipline");
                        var disciplineData = adddisciplineCollection.doc(row.QPID).set({
                            year: row.Datayear,
                            QPID: row.QPID,
                            disciplineDate: row.DateIssued,
                            Comment: row.Comment,
                            EmisStamp: row.EmisStamp,
                            Status: row.Status,
                            Action: row.ActionDescription,
                            IssuedBy: row.IssuedBy,
                            Suspension: row.Suspension,
                            Option: row.Option,
                            ConductLevel: row.ConductLevel,
                            ConductCode: row.ConductCode,
                            ConductDescription: row.ConductDescription,
                            ExpulsionDate: row.ExpulsionDate,
                            DemeritPoints: row.DemeritPoints,
                            Type: row.Type,
                            read: false
                        }, {
                            merge: true
                        }).catch(function(error) {
                            console.error("Error adding document: ", error);
                        });
                        trackEvent('Uploader', 'Import', 'Discipline', 'completed');
                    })
                    .on('end', () => {

                        fs.copyFile('C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-old.csv', (err) => {
                            if (err) {
                                console.log(err);
                            }
                            console.log('Rename complete!');
                        });
                        importerVal = importerVal + 1;
                        event.sender.send('importerVar', importerVal);
                        console.log("importerVal discipline=" + importerVal);
                    });
            }
        } else {
            //file exists
            readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline.csv', 'disciplinedata');
        }
    });
}

function importSchool(event) {
    var datenow = Date.now();
    var linecount = 0;
    //Check schooldata2 file exists
    fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old.csv', fs.F_OK, (err) => {
        if (err) {

            console.error("school old file doesnt exist");

            filePath = 'C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv';
            fileBuffer = fs.readFileSync(filePath);
            to_string = fileBuffer.toString();
            split_lines = to_string.split("\n");
            linecount = split_lines.length - 2;

            //just upload the original data
            fs.createReadStream('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv', 'utf-8')
                .pipe(csv())
                .on('data', (row) => {
                    currentlineschool = currentlineschool + 1;
                    //TODO add back in
                    addschooldata(row, event, linecount, currentlineschool);
                })
                .on('end', () => {
                    console.log('School CSV file successfully processed');
                    store.set('first-run', 'done');
                    fs.copyFile('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old.csv', (err) => {
                        if (err) {
                            console.log(err);
                        }
                        console.log('Rename complete!');
                    });
                });
        } else {
            //file exists
            readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv', 'schooldata');
        }
    });
}




function userbyUid(email) {
    firebase.auth().getUserByEmail(email)
        .then(function(userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
            console.log('Successfully fetched user data:', userRecord.toJSON());
            return userRecord.uid;
        })
        .catch(function(error) {
            console.log('Error fetching user data:', error);
        });
}

function addparentdata(row, event, tablefields) {
    let newparent = db.collection("users").doc(row.EmisStamp + "-" + row.ParentID).set({
        //Note change
        ParentID: row.ParentID,
        //Note change
        EMail: row.EMail,
        ParentID: row.ParentID,
        Display_Name: row.Display_Name,
        StreetAddress1: row.StreetAddress1,
        StreetAddress2: row.StreetAddress2,
        StreetAddress3: row.StreetAddress3,
        StreetCode: row.StreetCode,
        PostalAddress1: row.PostalAddress1,
        PostalAddress2: row.PostalAddress2,
        PostalAddress3: row.PostalAddress3,
        PostalCode: row.PostalCode,
        Tel1Code: row.Tel1Code,
        Tel1: row.Tel1,
        Tel2Code: row.Tel2Code,
        Tel2: row.Tel2,
        Tel3Code: row.Tel3Code,
        Tel3: row.Tel3,
        IDNumber: row.IDNumber,
        Title: row.Title,
        Homelanguage: row.Homelanguage,
        Status: row.StatusParent,
        EmisStamp: row.EmisStamp
    }, {
        merge: true
    }).then(function() {

        console.log("Parent created");
        if (store.get('first-run') != "done") {
            event.sender.send('ParentFinished', parentsadded);
        }
    });
    trackEvent('Uploader', 'Import', 'Parent', 'completed');

}

function addStudents(row, event, linecount, currentlinestudents) {
    var tablefields = row.EMail + '</p></div><div class="w-1/4 h-8 text-right "><p class="text-sm text-grey-400 text-left">' + row.LearnerFName + ' ' + row.LearnerSName;


    //var newChild = db.collection("children").doc(row.EmisStamp + "-" + row.ParentId + "-" + row.Learnerid).set({
    var newChild = db.collection("children").doc(row.EmisStamp + "-" + row.Learnerid).set({
        // add all other fields
        // ParentId: row.ParentId,
        Learnerid: row.Learnerid,
        LearnerFName: row.LearnerFName,
        LearnerSName: row.LearnerSName,
        Grade: row.Grade,
        ChildId: row.ChildId,
        Tel1Code: row.Tel1Code,
        Tel1: row.Tel1,
        Tel2Code: row.Tel2Code,
        Tel2: row.Tel2,
        Tel3Code: row.Tel3Code,
        Tel3: row.Tel3,
        Status: row.Status,
        BirthDate: row.BirthDate,
        IDNo: row.IDNo,
        Gender: row.Gender,
        HomeLanguage: row.HomeLanguage,
        Address1: row.Address1,
        Address2: row.Address2,
        Address3: row.Address3,
        AddressCode: row.AddressCode,
        //Email: row.Email,
        ClassName: row.ClassName,
        EmisStamp: row.EmisStamp
    }, {
        merge: true
    }).then(function() {
        console.log("Student Added");
        // Add childIDs to parent
        let parentUID = userbyUid(row.Email);
        // let ParentRef = db.collection('users').doc(parentUID);
        let ParentRef = db.collection('users').doc(row.EmisStamp + "-" + row.ParentId);
        let newChildID = ParentRef.get()
            .then((docSnapshot) => {
                if (docSnapshot.exists) {
                    ParentRef.onSnapshot((doc) => {
                        // do stuff with the data
                        // Atomically add a childID to Parent array field.
                        let addChildtoParent = ParentRef.update({
                            ChildId: firebase.firestore.FieldValue.arrayUnion(row.EmisStamp + "-" + row.Learnerid)
                        });
                    });
                }

                //Send email to parent - if parent new..
                for (let parent of userobj) {
                    if (parent.EmisStamp == row.EmisStamp + "-" + row.ParentId) {
                        parent.child = row.LearnerFName;
                        emailoutinfo(parent);
                    }
                }
            });
        studentsadded = studentsadded + 1;

        //Add parentIDs to child
        let ParentChild = db.collection("children").doc(row.EmisStamp + "-" + row.Learnerid);
        let newParentID = ParentChild.get()
            .then((docSnapshot) => {
                if (docSnapshot.exists) {
                    ParentChild.onSnapshot((doc) => {
                        // do stuff with the data
                        // Atomically add a childID to Parent array field.
                        let addChildtoParent = ParentChild.update({
                            //ParentId: firebase.firestore.FieldValue.arrayUnion(parentUID)
                            ParentId: firebase.firestore.FieldValue.arrayUnion(row.EmisStamp + "-" + row.ParentId)
                        });
                    });
                }
                console.log("StudentParent Added");
            });
        studentnumberstext = currentlinestudents + ' of ' + linecount + '. ' + studentsadded + ' new student added or updated.';
        if (store.get('first-run') != "done") {
            event.sender.send('studentnumbersReply', studentnumberstext);
        }

    });
    trackEvent('Uploader', 'Import', 'Student', 'completed');

};

function addacademicdata(row, event, linecount, currentlineAD) {

    academicdataadded = academicdataadded + 1;
    academicdatatext = currentlineAD + ' of ' + linecount + '. ' + academicdataadded + ' new academic reports uploaded.';
    if (store.get('first-run') != "done") {
        event.sender.send('academicdataReply', academicdatatext);
    }
    //ParentID issues..
    var addAcademicsCollection = db.collection("children").doc(row.EmisStamp + "-" + row.AccessionNo).collection("academics");
    var addAcademicsDoc = addAcademicsCollection.doc('allAcademics');
    var addActivitiesCollection = addAcademicsDoc.collection('reports');
    var addData = addActivitiesCollection.doc('report-' + row.Datayear + "-" + row.Term + "-" + row.SubjectId).set({
        year: row.Datayear,
        LearnerID: row.LearnerID,
        AccessionNo: row.AccessionNo,
        SName: row.SName,
        FName: row.FName,
        Grade: row.Grade,
        Register: row.Register,
        SubjectId: row.SubjectId,
        subject: row.SubjectName,
        subjectShort: row.SubjectShort,
        term: row.Term,
        mark: row.Mark,
        totalMark: row.TotalMark,
        Rating: row.Rating,
        comment1: row.Comment1,
        EmisStamp: row.EmisStamp
    }, {
        merge: true
    }).catch(function(error) {
        console.error("Error adding document: ", error);
    });

    trackEvent('Uploader', 'Import', 'Report', 'completed');

};

//Datayear,Term,Learnerid,AccessionNo,SName,FName,Grade,Register,Subjectid,SubjectName,SubjectNameAfrs,SubjectShort,TaskID,TaskName,TaskTotal,TaskMark,ParentId,Status,EmisStamp,QPID
function addtaskdata(row, event, linecount, currentlineAT) {

    academictasksadded = academictasksadded + 1;
    academictasktext = currentlineAT + ' of ' + linecount + '. ' + academictasksadded + ' new academic tasks uploaded.';
    if (store.get('first-run') != "done") {
        event.sender.send('academictasksReply', academictasktext);
    }
    var addAcademicsCollection = db.collection("children").doc(row.EmisStamp + "-" + row.AccessionNo).collection("academics");
    var addAcademicsDoc = addAcademicsCollection.doc('allAcademics');
    var addActivitiesCollection = addAcademicsDoc.collection('tasks');
    var addData = addActivitiesCollection.doc('task-' + row.Datayear + "-" + row.Term + "-" + row.Subjectid + "-" + row.TaskID).set({
        year: row.Datayear,
        LearnerID: row.Learnerid,
        AccessionNo: row.AccessionNo,
        SName: row.SName,
        FName: row.FName,
        Grade: row.Grade,
        Register: row.Register,
        subjectId: row.Subjectid,
        subject: row.SubjectName,
        subjectNameAfrs: row.SubjectNameAfrs,
        subjectShort: row.SubjectShort,
        TaskID: row.TaskID,
        task: row.TaskName,
        SubjectName: row.SubjectName,
        mark: row.TaskMark,
        taskTotal: row.TaskTotal,
        term: row.Term,
        EmisStamp: row.EmisStamp
    }, {
        merge: true
    }).catch(function(error) {
        console.error("Error adding document: ", error);
    });
    trackEvent('Uploader', 'Import', 'Task', 'completed');

};

// TODO add absences.. 

function addabsencedata(row, event, linecount, currentlineAbsence) {


    academicabsencesadded = academicabsencesadded + 1;
    academicabsencetext = currentlineAbsence + ' of ' + linecount + '. ' + academicabsencesadded + ' new academic absences uploaded.';
    if (store.get('first-run') != "done") {
        event.sender.send('academicabsencesReply', academicabsencetext);
    }
    var addAbsenceCollection = db.collection("children").doc(row.EmisStamp + "-" + row.AccessionNo).collection("absences");
    var absencedatestripped = row.AbsentDate.replace(/\D/g, '');
    var AbsenceData = addAbsenceCollection.doc('absence-' + row.DataYear + "-" + absencedatestripped).set({
        year: row.DataYear,
        QPID: row.QPID,
        AbsenceDate: row.AbsentDate,
        Reason: row.Reason,
        EmisStamp: row.EmisStamp,
        Status: row.Status,
        read: false
    }, {
        merge: true
    }).catch(function(error) {
        console.error("Error adding document: ", error);
    });
    trackEvent('Uploader', 'Import', 'Absence', 'completed');

};

//QPID, RecordID, Datayear, LearnerID, AccessionNo, ParentId, SName, FName, Comment, ConductLevel, ConductCode, ConductDescription, IssuedBy, Suspension, Option, ExpulsionDate, DemeritPoints, Status, DateIssued, Type, ActionDescription, EmisStamp

function adddisciplinedata(row, event, linecount, currentlineDiscipline) {

    academicdisciplineadded = academicdisciplineadded + 1;
    academicdisciplinetext = currentlineDiscipline + ' of ' + linecount + '. ' + academicdisciplineadded + ' new academic discipline uploaded.';
    if (store.get('first-run') != "done") {
        event.sender.send('academicdisciplineReply', academicdisciplinetext);
    }
    var adddisciplineCollection = db.collection("children").doc(row.EmisStamp + "-" + row.AccessionNo).collection("discipline");
    var disciplineData = adddisciplineCollection.doc(row.QPID).set({
        year: row.Datayear,
        QPID: row.QPID,
        disciplineDate: row.DateIssued,
        Comment: row.Comment,
        EmisStamp: row.EmisStamp,
        Status: row.Status,
        Action: row.ActionDescription,
        IssuedBy: row.IssuedBy,
        Suspension: row.Suspension,
        Option: row.Option,
        ConductLevel: row.ConductLevel,
        ConductCode: row.ConductCode,
        ConductDescription: row.ConductDescription,
        ExpulsionDate: row.ExpulsionDate,
        DemeritPoints: row.DemeritPoints,
        Type: row.Type,
        read: false
    }, {
        merge: true
    }).catch(function(error) {
        console.error("Error adding document: ", error);
    });
    trackEvent('Uploader', 'Import', 'Discipline', 'completed');

};
// Datayear,LearnerID,AccessionNo,SName,FName,Grade,Register,SubjectId,SubjectName,SubjectShort,Term,Mark,TotalMark,Rating,Comment1,EmisStamp
function addschooldata(row, event, linecount, currentline) {
    var grades = row.Grades;
    var classes = row.Classes;
    var gradeabv = grades.slice(0, -1);
    var classesabv = classes.slice(0, -1);
    var EmisCode = row.EmisCode;

    var newSchool = db.collection("schools").doc(row.EmisCode).set({
        EmisCode: row.EmisCode, // amend to parent.id
        ProvincialDep: row.ProvincialDep,
        SchoolName: row.SchoolName,
        Address1: row.Address1,
        Address2: row.Address2,
        Address3: row.Address3,
        AddressCode: row.AddressCode,
        PostAddress1: row.PostAddress1,
        PostAddress2: row.PostAddress2,
        PostAddress3: row.PostAddress3,
        PostCode: row.PostCode,
        Tel1Code: row.TelCode1,
        Telephone1: row.Telephone1,
        Tel2Code: row.TelCode2,
        Telephone2: row.Telephone2,
        Telcode3: row.Telcode3,
        Telephone3: row.Telephone3,
        ContactPerson: row.ContactPerson,
        EMail: row.EMail,
        EmisStamp: row.EmisStamp,
        grades: gradeabv,
        classes: classesabv
    }, {
        merge: true
    });
    var school_name = encodeURIComponent(row.SchoolName);
    var school_email = row.EMail;
    var gradesarr = gradeabv.split('|');
    var classesarr = classesabv.split('|');

    var addschoolurl = "https://quickparentapp.com/qp/userplus/addSchool/?key=qqz79j9RTXZBZVX9J5ga2k4u6m3FdM&email=" + school_email + "&school_name=" + school_name + "&gradesarray=" + gradesarr + "&classesarray=" + classesarr + "&EmisCode=" + EmisCode;
    console.log(addschoolurl);
    //TODO


    https.get(addschoolurl, (resp) => {
        let data = '';

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // The whole response has been received. Print out the result.
        resp.on('end', () => {
            console.log("json parse date")
            console.log(JSON.parse(data));
            //TODO 

            event.sender.send('schoolregistered', JSON.parse(data));

            trackEvent('Uploader', 'Import', 'School', 'completed');

        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}


//Generate randomm Password
function generatePassword() {
    var randPassword = Array;
    randPassword = Array(10).fill("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz").map(function(x) {
        return x[Math.floor(Math.random() * x.length)]
    }).join('');
    return randPassword;
}

//READ FILES and COMPARE - then update..
function readcompareupdate(firstfile, secondfile, thirdfile, typeofdata) {
    fs.access(firstfile, fs.F_OK, (err) => {
        if (err) {
            console.log('exit here now');

        } else {
            fs.access(secondfile, fs.F_OK, (err) => {
                if (err) {
                    console.log('exit here now 2');
                } else {
                    var intialFile = fs.readFileSync(firstfile);
                    var secondaryFile = fs.readFileSync(secondfile);
                    console.log("read");
                    var user1obj = csv2obj(intialFile, 0); // old file - first column value
                    var user2obj = csv2obj(secondaryFile, 0); // New file - first column value


                    var entirediff = detailedDiff(user1obj, user2obj);
                    console.log(entirediff);



                    switch (typeofdata) {
                        case "userdata":
                            parentsInteger = 0;
                            if (!isEmpty(entirediff.added)) {
                                console.log("userdata added greater than 0");
                                parentsInteger = parentsInteger + Object.entries(entirediff.added).length;
                                Object.entries(entirediff.added).forEach(([key]) => {
                                    addparentdata(user2obj[key]);
                                });
                            }

                            // TODO - discuss
                            if (!isEmpty(entirediff.deleted)) {
                                console.log("deleted greater than 0");
                                Object.entries(entirediff.deleted).forEach(([key]) => {
                                    // add function for value here
                                });
                            }

                            if (!isEmpty(entirediff.updated)) {
                                console.log("updated greater than 0");
                                parentsInteger = parentsInteger + Object.entries(entirediff.updated).length;
                                Object.entries(entirediff.updated).forEach(([key]) => {
                                    // add function for value here
                                    console.log("try add parent update");
                                    addparentdata(user2obj[key]);
                                });
                            }


                            break;
                        case "studentdata":
                            studentInteger = 0;
                            if (!isEmpty(entirediff.added)) {
                                console.log("added greater than 0");
                                studentInteger = studentInteger + Object.entries(entirediff.added).length;
                                Object.entries(entirediff.added).forEach(([key]) => {
                                    // add function for value here
                                    addStudents(user2obj[key]);
                                });
                            }

                            // TODO - discuss
                            if (!isEmpty(entirediff.deleted)) {
                                console.log("deleted greater than 0");
                                Object.entries(entirediff.deleted).forEach(([key]) => {
                                    // add function for value here
                                });
                            }

                            if (!isEmpty(entirediff.updated)) {
                                console.log("updated greater than 0");
                                studentInteger = studentInteger + Object.entries(entirediff.updated).length;
                                Object.entries(entirediff.updated).forEach(([key]) => {
                                    // add function for value here
                                    addStudents(user2obj[key]);
                                });
                            }
                            //      addchilddata(row);
                            break;
                        case "reportdata":
                            reportsInteger = 0;
                            if (!isEmpty(entirediff.added)) {
                                console.log("added greater than 0");
                                reportsInteger = reportsInteger + Object.entries(entirediff.added).length;
                                Object.entries(entirediff.added).forEach(([key]) => {
                                    // add function for value here
                                    addacademicdata(user2obj[key]);
                                });
                            }

                            // TODO - discuss
                            if (!isEmpty(entirediff.deleted)) {
                                console.log("deleted greater than 0");
                                Object.entries(entirediff.deleted).forEach(([key]) => {

                                    // add function for value here
                                });
                            }

                            if (!isEmpty(entirediff.updated)) {
                                console.log("updated greater than 0");
                                reportsInteger = reportsInteger + Object.entries(entirediff.updated).length;
                                Object.entries(entirediff.updated).forEach(([key]) => {
                                    // add function for value here
                                    addacademicdata(user2obj[key]);
                                });
                            }
                            break;
                        case "taskdata":
                            tasksInteger = 0;
                            if (!isEmpty(entirediff.added)) {
                                console.log("added greater than 0");
                                tasksInteger = tasksInteger + Object.entries(entirediff.added).length;
                                Object.entries(entirediff.added).forEach(([key]) => {
                                    // add function for value here
                                    addtaskdata(user2obj[key]);
                                });
                            }

                            // TODO - discuss
                            if (!isEmpty(entirediff.deleted)) {
                                console.log("deleted greater than 0");
                                Object.entries(entirediff.deleted).forEach(([key]) => {
                                    // add function for value here
                                });
                            }

                            if (!isEmpty(entirediff.updated)) {
                                console.log("updated greater than 0");
                                tasksInteger = tasksInteger + Object.entries(entirediff.updated).length;
                                Object.entries(entirediff.updated).forEach(([key]) => {
                                    // add function for value here
                                    addtaskdata(user2obj[key]);
                                });
                            }

                            break;
                        case "absencedata":
                            absencesInteger = 0;
                            if (!isEmpty(entirediff.added)) {
                                console.log("added greater than 0");
                                absencesInteger = absencesInteger + Object.entries(entirediff.added).length;
                                Object.entries(entirediff.added).forEach(([key]) => {
                                    // add function for value here
                                    addabsencedata(user2obj[key]);

                                });
                            }

                            // TODO - discuss
                            if (!isEmpty(entirediff.deleted)) {
                                console.log("deleted greater than 0");
                                Object.entries(entirediff.deleted).forEach(([key]) => {
                                    // add function for value here
                                });
                            }

                            if (!isEmpty(entirediff.updated)) {
                                console.log("updated greater than 0");
                                absencesInteger = absencesInteger + Object.entries(entirediff.updated).length;

                                Object.entries(entirediff.updated).forEach(([key]) => {
                                    // add function for value here
                                    addabsencedata(user2obj[key]);

                                });
                            }

                            break;

                        case "disciplinedata":
                            disciplineInteger = 0;
                            if (!isEmpty(entirediff.added)) {
                                console.log("added greater than 0");
                                disciplineInteger = disciplineInteger + Object.entries(entirediff.added).length;

                                Object.entries(entirediff.added).forEach(([key]) => {
                                    // add function for value here
                                    adddisciplinedata(user2obj[key]);

                                });
                            }

                            // TODO - discuss
                            if (!isEmpty(entirediff.deleted)) {
                                console.log("deleted greater than 0");
                                Object.entries(entirediff.deleted).forEach(([key]) => {
                                    // add function for value here
                                });
                            }

                            if (!isEmpty(entirediff.updated)) {
                                console.log("updated greater than 0");
                                disciplineInteger = disciplineInteger + Object.entries(entirediff.updated).length;

                                Object.entries(entirediff.updated).forEach(([key]) => {
                                    // add function for value here
                                    adddisciplinedata(user2obj[key]);

                                });
                            }

                            break;
                        case "schooldata":
                            //likely to never update.                                    
                            if (!isEmpty(entirediff.added)) {
                                console.log("added greater than 0");
                                Object.entries(entirediff.added).forEach(([key]) => {
                                    // add function for value here
                                    addschooldata(user2obj[key]);
                                });
                            }

                            // TODO - discuss
                            if (!isEmpty(entirediff.deleted)) {
                                console.log("deleted greater than 0");
                                Object.entries(entirediff.deleted).forEach(([key]) => {
                                    // add function for value here
                                });
                            }

                            if (!isEmpty(entirediff.updated)) {
                                console.log("updated greater than 0");
                                Object.entries(entirediff.updated).forEach(([key]) => {
                                    // add function for value here
                                    addschooldata(user2obj[key]);
                                });
                            }

                            break;
                    }
                    renamefiles(thirdfile, secondfile);
                }

            })
        }
    })


}

function renamefiles(firstfile, secondfile) {
    fs.access(firstfile, fs.F_OK, (err) => {
        if (err) {
            console.log(firstfile + ' doesn\'t exit');
        } else {
            fs.copyFile(secondfile, firstfile, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("copy replace " + secondfile + " -> " + firstfile);
                }
            });
        }
    })
}

//csv to obj
function csv2obj(csvfile, indexfield) {
    console.log(csvfile);
    var filelines = csvfile.toString().split('\r\n');
    //console.log("user1");
    //console.log(filelines);
    var csvobj = [];
    var headers = filelines[0].split(",");
    //var hl = headers.length - 1;
    for (var i = 1; i < filelines.length; i++) {
        var obj = {};
        var currentline = filelines[i].split(",");
        for (var j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j];
        }
        csvobj[currentline[indexfield]] = obj;
        // user1obj.push(obj);
    }
    return csvobj;
}

function isEmpty(obj) {
    return !obj || Object.keys(obj).length === 0;
}

ipcMain.on('online-status-changed', (event, status) => {
    console.log(status)
})


ipc.on('startcontinuous', function(event, data) {
    isRunning('QuickParentConverter.exe', (status) => {
        console.log(status); // true|false
        if (status == true) {
            mainWindow.webContents.send('start', 'go');
        } else {
            mainWindow.webContents.send('waitstart', `wait`);
            var myTimer = setInterval(function() {
                isRunning('QuickParentConverter.exe', (status) => {
                    console.log(status); // true|false
                    if (status == true) {
                        mainWindow.webContents.send('start', 'go');
                        clearInterval(myTimer);
                    }
                })
            }, 10000);
        }
    })
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo-old2.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-old2.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-old2.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-old2.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-old2.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-old2.csv");
    renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old2.csv");

    lastrunvals(event);
});

async function operation() {
    return new Promise(function(resolve, reject) {
        // schooluserId = nodeStorage.getItem('userid') || 
        setschooluserId('Uploader', 'Continuous', 'CSV data', 'start');
        // // (re)save the userid, so it persists for the next app session.
        // nodeStorage.setItem('userid', schooluserId);
        // usr = ua('UA-156149023-1', schooluserId);

        console.log("start y");
        // Run extractor (silently)


        //check Parents
        fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo-old.csv', fs.F_OK, (err) => {
            if (err) {
                console.error("parent old file doesnt exist");
                console.log(err);
                renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo.csv");
            } else {
                fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo.csv', fs.F_OK, (err) => {
                    if (err) {
                        console.log('parent file doesnt exist');
                    } else {
                        readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\parentInfo-old2.csv', 'userdata');


                    }


                });
            }
        });
        //Check Students
        fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-old.csv', fs.F_OK, (err) => {
            if (err) {
                //file doesn't exists
                console.error("student old file doesnt exist");
                renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo.csv");

            } else {
                fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo.csv', fs.F_OK, (err) => {
                    if (err) {
                        console.log('student file doesnt exist');
                    } else {
                        readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerInfo-old2.csv', 'studentdata');
                    }
                });
            }
        });
        //Check Reports
        fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-old.csv', fs.F_OK, (err) => {
            if (err) {
                console.error("report old file doesnt exist");
                renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData.csv");

            } else {
                fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData.csv', fs.F_OK, (err) => {
                    if (err) {
                        console.log('report file doesnt exist');
                    } else {
                        readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerReportData-old2.csv', 'reportdata');
                    }
                });
            }
        });
        //Check Tasks
        fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-old.csv', fs.F_OK, (err) => {
            var linecount = 0;
            if (err) {
                //file doesn't exists
                console.error("tasks old file doesnt exist");
                renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData.csv");

            } else {
                fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData.csv', fs.F_OK, (err) => {
                    if (err) {
                        console.log('tasks file doesnt exist');
                    } else {
                        readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerTasksData-old2.csv', 'taskdata');
                    }
                });
            }
        });
        //Check Absences
        fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-old.csv', fs.F_OK, (err) => {
            var linecount = 0;
            if (err) {
                //file doesn't exists
                console.error("absence data old file doesnt exist");
                renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount.csv");

            } else {
                fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount.csv', fs.F_OK, (err) => {
                    if (err) {
                        console.log('absence file doesnt exist');
                    } else {
                        readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerAbsentCount-old2.csv', 'absencedata');
                    }
                });
            }
        });
        //Check Discipline
        fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-old.csv', fs.F_OK, (err) => {
            var linecount = 0;
            if (err) {
                //file doesn't exists
                console.error("absence data old file doesnt exist");
                renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline.csv");

            } else {
                fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline.csv', fs.F_OK, (err) => {
                    if (err) {
                        console.log('absence file doesnt exist');
                    } else {
                        readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\learnerDiscipline-old2.csv', 'disciplinedata');
                    }
                });
            }
        });
        //Check School details
        fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old.csv', fs.F_OK, (err) => {
            if (err) {

                console.error("school old file doesnt exist");
                renamefiles("C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old.csv", "C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv");

            } else {
                fs.access('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv', fs.F_OK, (err) => {
                    if (err) {
                        console.log('school file doesnt exist');
                    } else {
                        readcompareupdate('C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo.csv', 'C:\\QuickSuite\\QuickParent\\CSVData\\schoolInfo-old2.csv', 'schooldata');
                    }

                });
            }
        });
        setTimeout(() => {
            resolve({ parentsInteger, studentInteger, reportsInteger, tasksInteger, absencesInteger, disciplineInteger })
        }, 10000);

    })
}

async function lastrunvals(event) {

    var a = await operation()
    console.log(a);
    // Using Date() function 

    var today = new Date();
    var hh = today.getHours();
    var mins = (today.getMinutes() < 10 ? '0' : '') + today.getMinutes();
    var dd = today.getDate();

    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }
    var yyyy = today.getFullYear();
    var today = dd + '/' + mm + '/' + yyyy + " " + hh + ":" + mins;

    var lastrun_stats = {
        parents: a.parentsInteger,
        students: a.studentInteger,
        reports: a.reportsInteger,
        tasks: a.tasksInteger,
        absences: a.absencesInteger,
        discipline: a.disciplineInteger,
        school: randomInteger(0, 20),
        lastrun_td: today
    };
    console.log(lastrun_stats);
    event.sender.send('lastrun', lastrun_stats);


}

function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const isRunning = (query, cb) => {
    let platform = process.platform;
    let cmd = '';
    switch (platform) {
        case 'win32':
            cmd = `tasklist`;
            break;
        case 'darwin':
            cmd = `ps -ax | grep ${query}`;
            break;
        case 'linux':
            cmd = `ps -A`;
            break;
        default:
            break;
    }
    execFile(cmd, (err, stdout, stderr) => {
        cb(stdout.toLowerCase().indexOf(query.toLowerCase()) > -1);
    });
}

function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

async function BatchedWrites(datas, datacollection) {

    let batches = [];
    let batch = db.batch();
    let count = 0;
    while (datas.length) {
        var docid = datas[0]['docid'];
        batch.set(db.collection(datacollection).doc(docid), datas.shift());
        if (++count >= 500 || !datas.length) {
            batches.push(batch.commit());
            batch = db.batch();
            count = 0;
        }
    }
    const _dataload = await Promise.all(batches);
}

async function BatchedWritesreports(datas, datacollection, datacollection2) {

    let batches = [];
    let batch = db.batch();
    let count = 0;
    while (datas.length) {
        var docid = datas[0]['docid'];
        var docfinal = datas[0]['docfinal'];
        batch.set(db.collection(datacollection).doc(docid).collection("academics").doc("allAcademics").collection(datacollection2).doc(docfinal), datas.shift());
        if (++count >= 500 || !datas.length) {
            batches.push(batch.commit());
            batch = db.batch();
            count = 0;
        }
    }
    await Promise.all(batches);
}

async function BatchedUpdates(datas, datacollection) {
    let batches = [];
    let batch = db.batch();
    let count = 0;
    while (datas.length) {

        batch.update(db.collection(datacollection).doc(datadoc), datas.shift());
        if (++count >= 500 || !datas.length) {
            batches.push(batch.commit());
            batch = db.batch();
            count = 0;
        }
    }
    await Promise.all(batches);
}

// function listAllUsers(nextPageToken) {
//     // List batch of users, 1000 at a time.
//     firebase.auth().listUsers(1000, nextPageToken)
//         .then(function(listUsersResult) {
//             listUsersResult.users.forEach(function(userRecord) {
//                 console.log('user', userRecord.toJSON());
//             });
//             if (listUsersResult.pageToken) {
//                 // List next batch of users.
//                 listAllUsers(listUsersResult.pageToken);
//             }
//         })
//         .catch(function(error) {
//             console.log('Error listing users:', error);
//         });
// }
// // Start listing users from the beginning, 1000 at a time.
// listAllUsers();