# create-users-debug

## To install:

- npm i
- run QP_CSV_Generator.msi and install to default location (if changed, it will require a few lines of code in main.js to be changed - C:/QuickSuite/QuickParent/ is default)
- extract mdb files from .rar in "test dbs" folder

## To run:

- npm start
- click through buttons..
- when CSV Generator software runs, select mdb in "test dbs" folder - two options small and large mdb
- once csvs generated, continue - Parent import is currently commented out in the code, so simply continue.
- StudentImport runs all functions in parallel (current setup lines 480+ in main.js)

ctrl+c exits app..

## To re-run/retest:

- remove all CSVs from CSVData folder
- ensure QP_CSV_Generator is completely exited/closed - it runs in the background, so exit from taskbar
- ensure "children" collection in Firestore is empty/deleted

## Key information:
